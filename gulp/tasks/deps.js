var gulp       = require('gulp');
var inject     = require('gulp-inject');
var wiredep    = require('wiredep').stream;
var config     = require('../config').deps;

var source = gulp.src(config.assets, {read: false});

gulp.task('deps', function () {
  gulp.src(config.src)
    .pipe(wiredep(config.wiredep))
    .pipe(inject(source, config.inject))
    .pipe(gulp.dest(config.dest));
});
