var srcAssets         = 'luggageshare/static/src';
var devAssets         = 'luggageshare/static/dev';
var prodAssets        = 'luggageshare/static/prod';
var srcTemplates      = 'luggageshare/templates';
var devTemplates      = 'luggageshare/templates_dev';
var prodTemplates     = 'luggageshare/templates_prod';
var baseScss          = srcAssets + '/scss/style.scss';
var baseTemplate      = srcTemplates + '/base.html';

module.exports = {
  browsersync: {
    dev: {
      port: 8000,
      ui: {
        port: 8080
      },
      proxy: 'localhost:8010',
      open: false,
      files: [
        devAssets + '/css/*.css',
      ]
    }
  },
  delete: {
    dev: [devAssets, devTemplates],
    prod: [devAssets, prodAssets, prodTemplates]
  },
  deps: {
    src: baseTemplate,
    dest: devTemplates,
    assets: [devAssets + '/js/*.js', devAssets + '/css/*.css'],
    wiredep: {
      ignorePath: '../../bower_components/',
      fileTypes: {
        html: {
          replace: {
            js: '<script src="\{% static \'{{filePath}}\' %\}"></script>',
            css: '<link rel="stylesheet" href="\{% static \'{{filePath}}\' %\}">'
          }
        }
      }
    },
    inject: {
      addRootSlash: false,
      ignorePath: 'luggageshare/static/',
      transform: function (filepath) {
        if (filepath.slice(-3) === '.js') {
          return '<script src="\{% static \''+ filepath +'\' %\}"></script>';
        } else if (filepath.slice(-4) === '.css') {
          return '<link rel="stylesheet" href="\{% static \'' + filepath + '\' %\}">';
        } else {
          return inject.transform.apply(inject.transform, arguments);
        }
      }
    }
  },
  inject: {
    src: baseTemplate,
    dest: prodTemplates,
    assets: [prodAssets + '/**/*.js', prodAssets + '/**/*.css'],
    options: {
      addRootSlash: false,
      ignorePath: 'luggageshare/static/',
      transform: function (filepath) {
        if (filepath.slice(-3) === '.js') {
          return '<script src="\{% static \''+ filepath +'\' %\}"></script>';
        } else if (filepath.slice(-4) === '.css') {
          return '<link rel="stylesheet" href="\{% static \'' + filepath + '\' %\}">';
        } else {
          return inject.transform.apply(inject.transform, arguments);
        }
      }
    }
  },
  optimize: {
    dest: prodAssets,
    mainBowerFiles: {},
    js:{
      src: devAssets + '/js/*.js',
      fileName: 'bundle.js',
      concat: {},
      uglify: {}
    },
    css: {
      src: devAssets + '/css/*.css',
      fileName: 'bundle.css',
      concat: {},
      cssnano: {}
    }
  },
  scripts: {
    src: srcAssets + '/js/*.js',
    dest: devAssets + '/js'
  },
  styles: {
    src: baseScss,
    dest: devAssets + '/css/',
    postcss: {
      autoprefixer: {
        browsers: [
          'last 2 versions',
          'safari 5',
          'ie 8',
          'ie 9',
          'opera 12.1',
          'ios 6',
          'android 4'
        ],
        cascade: true
      },
      mqpacker: {}
    },
    wiredep: {
      options: {}
    }
  },
  watch: {
    scss: srcAssets  + '/scss/*.scss',
    js: srcAssets + '/js/*.js'
  }
};
