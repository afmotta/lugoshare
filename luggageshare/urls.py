# coding: utf-8
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin


urlpatterns = [
    url(r'^', include('luggageshare.apps.luggage.urls', namespace='luggage')),
    url('^profile/', include('luggageshare.apps.profile.urls', namespace='profile')),
    url(r'^admin/', include(admin.site.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'LuggageShare admin'
