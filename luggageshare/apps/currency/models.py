from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _

from model_utils.models import TimeStampedModel
from six import python_2_unicode_compatible


@python_2_unicode_compatible
class Currency(TimeStampedModel):
    code = models.CharField(
        max_length=3,
        unique=True,
        verbose_name=_('code')
    )
    name = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=_('name')
    )
    exchange_rate = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        verbose_name=_('exchange rate of currency to EUR')
    )

    class Meta:
        verbose_name = _('currency')
        verbose_name_plural = _('currencies')
        ordering = ('code',)

    def __str__(self):
        return self.code
