from django.contrib import admin

from luggageshare.apps.currency.models import Currency


class CurrencyAdmin(admin.ModelAdmin):
    fields = (
        'created', 'modified', 'code', 'exchange_rate', 'name',
    )
    list_display = (
        'code', 'exchange_rate', 'name',
    )
    readonly_fields = ('created', 'modified')

admin.site.register(Currency, CurrencyAdmin)
