# coding: utf-8
from decimal import Decimal

from django.conf import settings

from luggageshare.apps.currency.models import Currency


class CurrencyConversionException(Exception):
    """
    Raised by conversion utility function when problems arise
    """


def get_rate(currency):
    """Returns the rate from the default currency to `currency`."""
    try:
        return Currency.objects.get(code=currency).exchange_rate
    except Currency.DoesNotExist:
        raise CurrencyConversionException(
            """
            Rate for %s do not exists.
            Please run python manage.py update_rates
            """.format(currency)
        )


def convert_money(amount, currency_from, currency_to):
    """
    Convert 'amount' from 'currency_from' to 'currency_to'
    """
    source = settings.BASE_CURRENCY

    # Get rate for currency_from.
    if source != currency_from:
        rate_from = get_rate(currency_from)
    else:
        # If currency from is the same as base currency its rate is 1.
        rate_from = Decimal(1)

    # Get rate for currency_to.
    rate_to = get_rate(currency_to)

    if isinstance(amount, float):
        amount = Decimal(amount).quantize(Decimal('.000001'))

    # After finishing the operation, quantize down final amount to two points.
    return ((amount / rate_from) * rate_to).quantize(Decimal("1.00"))

