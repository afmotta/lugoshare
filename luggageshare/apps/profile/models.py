# coding: utf-8
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext as _

from model_utils.models import TimeStampedModel

from luggageshare.apps.currency.models import Currency
from luggageshare.apps.luggage.models import Luggage


def get_avatar_upload_path(instance, filename):
    return 'avatar/{0}/{1}'.format(instance, filename)


@python_2_unicode_compatible
class Profile(TimeStampedModel):
    """
    Class used to extend django.contrib.auth.user
    """
    user = models.OneToOneField(
        User,
        verbose_name=_('User'),
        related_name='profile',
    )
    avatar = models.ImageField(
        verbose_name=_('avatar'),
        upload_to=get_avatar_upload_path,
        blank=True,
        null=True,
    )
    weight_mu = models.CharField(
        max_length=100,
        choices=Luggage.WEIGHT_MU,
        default='kg',
        verbose_name='weight measurement unit'
    )
    currency = models.ForeignKey(
        Currency,
        related_name='profiles',
        verbose_name='currency',
    )

    def __str__(self):
        if self.user and self.user.first_name and self.user.last_name:
            return '{} {}'.format(self.user.first_name, self.user.last_name)
        return self.user.username


@receiver(post_save, sender=User)
def user_post_save_handler(sender, **kwargs):
    user = kwargs.get('instance', None)
    if user:
        try:
            user.profile.save()
        except Profile.DoesNotExist:
            try:
                currency = Currency.objects.get(code='EUR')
            except Currency.DoesNotExist:
                currency = Currency.objects.first()
            Profile(user=user, currency=currency).save()
