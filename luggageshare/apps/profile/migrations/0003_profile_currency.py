# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-20 19:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('currency', '0001_initial'),
        ('profile', '0002_profile_weight_mu'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='currency',
            field=models.ForeignKey(default=32, on_delete=django.db.models.deletion.CASCADE, related_name='profiles', to='currency.Currency', verbose_name='currency'),
            preserve_default=False,
        ),
    ]
