from django.contrib import admin

from luggageshare.apps.profile.models import Profile

admin.site.register(Profile)
