# coding: utf-8

from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.template import loader
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.decorators import cache, debug
from django.views.generic import TemplateView, FormView, UpdateView

from luggageshare.apps.profile.forms import RegisterForm
from luggageshare.apps.profile.models import Profile


class ProfileView(TemplateView):
    template_name = 'profile/profile.html'

    def __init__(self, **kwargs):
        self.profile = None
        super(ProfileView, self).__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request.user, 'profile'):
            self.profile = request.user.profile
        else:
            return redirect('profile:login')
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(ProfileView, self).get_context_data(**kwargs)
        ctx['profile'] = self.profile
        return ctx


class RegisterView(FormView):
    form_class = RegisterForm
    success_url = '/'
    template_name = 'profile/register.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('profile:profile')
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save()
        self.send_confirmation_email(user)
        messages.add_message(
            self.request, messages.INFO,
            u"""
                Thanks for registering. An email has been sent to your address.
                Please click on the link in the email to confirm your address
            """
        )
        return super(RegisterView, self).form_valid(form)

    def send_confirmation_email(self, user):
        current_site = get_current_site(self.request)
        site_name = current_site.name
        domain = current_site.domain

        context = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'https' if self.request.is_secure() else 'http',
        }
        subject = loader.render_to_string('profile/register_confirm_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('profile/register_confirm_email.html', context)
        email_message = EmailMultiAlternatives(subject, body, 'info@luggageshare.com', [user.email])
        html_email = loader.render_to_string('profile/register_confirm_email.html', context)
        email_message.attach_alternative(html_email, 'text/html')

        email_message.send()


@debug.sensitive_post_parameters()
@cache.never_cache
def register_confirm(request, uidb64=None, token=None):
    assert uidb64 is not None and token is not None  # checked by URLconf

    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()

        return redirect('profile:register_confirm_done')

    return redirect('profile:login')


class RegisterConfirmDone(TemplateView):
    template_name = 'profile/register_confirm_done.html'


class ProfileUpdate(UpdateView):
    model = User
    fields = ['first_name', 'last_name', 'email']
    template_name = 'profile/update.html'
    success_url = reverse_lazy('profile:profile')

    def __init__(self, **kwargs):
        self.user = None
        super(ProfileUpdate, self).__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            self.user = request.user
        else:
            redirect('profile:login')
        return super(ProfileUpdate, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return self.user


class ProfilePreferences(UpdateView):
    model = Profile
    fields = ['weight_mu', 'currency']
    template_name = 'profile/preferences.html'
    success_url = reverse_lazy('profile:profile')

    def __init__(self, **kwargs):
        self.profile = None
        super(ProfilePreferences, self).__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated() and hasattr(request.user, 'profile'):
            self.profile = request.user.profile
        else:
            redirect('profile:login')
        return super(ProfilePreferences, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return self.profile
