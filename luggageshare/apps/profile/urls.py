# coding: utf-8

from django.conf.urls import url
from django.contrib.auth import views as auth_views

from luggageshare.apps.profile import views

urlpatterns = [
    url(
        r'^login/$',
        auth_views.login,
        {
            'template_name': 'profile/login.html'
        },
        name='login'
    ),
    url(
        r'^logout/$',
        auth_views.logout,
        {
            'next_page': '/'
        },
        name='logout'
    ),
    url(
        r'^password_change/$',
        auth_views.password_change,
        {
            'template_name': 'profile/password_change.html',
            'post_change_redirect': 'profile:password_change_done'
        },
        name='password_change'
    ),
    url(
        r'^password_change/done/$',
        auth_views.password_change_done,
        {
            'template_name': 'profile/password_change_done.html'
        },
        name='password_change_done'
    ),
    url(
        r'^password_reset/$',
        auth_views.password_reset,
        {
            'template_name': 'profile/password_reset.html',
            'post_reset_redirect': 'profile:password_reset_done',
            'email_template_name': 'profile/password_reset_email.html',
            'from_email': 'info@luggageshare.com'
        },
        name='password_reset'
    ),
    url(
        r'^password_reset/done/$',
        auth_views.password_reset_done,
        {
            'template_name': 'profile/password_reset_done.html'
        },
        name='password_reset_done'
    ),
    url(
        r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {
            'template_name': 'profile/password_reset_confirm.html',
            'post_reset_redirect': 'profile:password_reset_complete'
        },
        name='password_reset_confirm'
    ),
    url(
        r'^reset/done/$',
        auth_views.password_reset_complete,
        {
            'template_name': 'profile/password_reset_complete.html',
        },
        name='password_reset_complete'
    ),
    url(
        r'^register/$',
        views.RegisterView.as_view(),
        name='register'
    ),
    url(
        r'^confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.register_confirm,
        name='register_confirm'
    ),
    url(
        r'^confirm/done/$',
        views.RegisterConfirmDone.as_view(),
        name='register_confirm_done'
    ),
    url(
        r'^update/$',
        views.ProfileUpdate.as_view(),
        name='update'
    ),
    url(
        r'^preferences/$',
        views.ProfilePreferences.as_view(),
        name='preferences'
    ),
    url(
        r'$',
        views.ProfileView.as_view(),
        name='profile'
    )
]
