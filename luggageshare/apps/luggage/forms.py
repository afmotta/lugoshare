# coding: utf-8
from django import forms

from luggageshare.apps.luggage.models import Luggage


class LuggageForm(forms.ModelForm):
    class Meta:
        model = Luggage
        fields = (
            'departure',
            'departure_date',
            'arrival',
            'arrival_date',
            'size',
            'weight',
            'weight_mu',
            'price',
            'currency',
            'note'
        )
