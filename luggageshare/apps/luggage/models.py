# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from decimal import Decimal

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext as _
from model_utils import Choices
from model_utils.fields import StatusField, MonitorField
from model_utils.models import TimeStampedModel
from six import python_2_unicode_compatible

from luggageshare.apps.currency.models import Currency
from luggageshare.apps.currency.utils import convert_money


@python_2_unicode_compatible
class Luggage(TimeStampedModel):

    STATUS = Choices(
        ('draft', _('draft')),
        ('published', _('published')),
        ('accepted', _('accepted')),
        ('done', _('done')),
        ('removed', _('removed')),
    )

    SIZES = Choices(
        ('extra-small', _('extra-small')),
        ('small', _('small')),
        ('medium', _('medium')),
        ('large', _('large')),
        ('extra-large', _('extra-large'))
    )

    WEIGHT_MU = Choices(
        'kg', 'lbs',
    )

    status = StatusField()
    published_at = MonitorField(monitor='status', when=['published'])
    accepted_at = MonitorField(monitor='status', when=['accepted'])
    done_at = MonitorField(monitor='status', when=['done'])
    removed_at = MonitorField(monitor='status', when=['removed'])

    user = models.ForeignKey(
        User,
        related_name='luggage',
        verbose_name=_('user')
    )
    departure = models.CharField(
        max_length=255,
        verbose_name=_('departure')
    )
    departure_date = models.DateTimeField(
        verbose_name=_('departure date')
    )
    arrival = models.CharField(
        max_length=255,
        verbose_name=_('arrival')
    )
    arrival_date = models.DateTimeField(
        verbose_name=_('arrival date')
    )
    size = models.CharField(
        choices=SIZES,
        max_length=255,
        verbose_name=_('size')
    )
    weight = models.DecimalField(
        decimal_places=1,
        max_digits=5,
        verbose_name=_('weight'),
        validators=[MinValueValidator(0)]
    )
    weight_mu = models.CharField(
        choices=WEIGHT_MU,
        default=WEIGHT_MU.kg,
        max_length=20,
        verbose_name=_('weight measure unit')
    )
    price = models.DecimalField(
        decimal_places=1,
        max_digits=10,
        verbose_name=_('price'),
        validators=[MinValueValidator(0)]
    )
    currency = models.ForeignKey(
        Currency,
        related_name='luggage',
        verbose_name=_('currency')
    )
    note = models.TextField(
        verbose_name=_('note'),
        blank=True, null=True,
    )

    class Meta:
        verbose_name = _('luggage')
        verbose_name_plural = _('luggage')
        ordering = ('-created', 'departure_date', 'departure', 'arrival_date', 'arrival')

    def __str__(self):
        return 'From {} to {}'.format(self.departure, self.arrival)

    def get_departure_date(self):
        return self.departure_date.strftime("%d/%m/%Y %H:%M")

    def get_arrival_date(self):
        return self.arrival_date.strftime("%d/%m/%Y %H:%M")

    def get_detail_url(self):
        return reverse('luggage:luggage_detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('luggage:luggage_update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('luggage:luggage_delete', kwargs={'pk': self.pk})

    def delete_luggage(self):
        self.status = 'removed'
        self.save()

    def convert_weight(self, measure_unit='kg'):
        if measure_unit not in ['kg', 'lbs']:
            raise ValueError("weight can be converted in kg or lbs")
        if measure_unit == self.weight_mu:
            return round(self.weight, 2)
        elif measure_unit == 'lbs' and self.weight_mu == 'kg':
            return round(self.weight * Decimal(0.4535923), 2)
        elif measure_unit == 'kg' and self.weight_mu == 'lbs':
            return round(self.weight * Decimal(2.204622), 2)
        else:
            return 0

    def convert_price(self, currency_to):
        return convert_money(self.price, self.currency, currency_to)
