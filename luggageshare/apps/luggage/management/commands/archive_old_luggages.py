# coding: utf-8

from datetime import datetime
from django.core.management.base import BaseCommand
from luggageshare.apps.luggage.models import Luggage


class Command(BaseCommand):
    def handle(self, *args, **options):
        Luggage.objects.filter(departure_date__lt=datetime.now()).update(status='removed')
