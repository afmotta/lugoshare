# coding: utf-8

import json
import requests

from django.core.management.base import BaseCommand

from luggageshare.apps.currency.models import Currency


class Command(BaseCommand):

    def handle(self, *args, **options):
        r = requests.get('http://api.fixer.io/latest')
        rates = json.loads(r.content).get('rates', None)
        if rates:
            try:
                for code, rate in rates.iteritems():
                    try:
                        currency = Currency.objects.get(code=code)
                    except Currency.DoesNotExist:
                        currency = Currency(code=code)
                    currency.exchange_rate = rate
                    currency.save()

            except TypeError:
                self.stdout.write(self.style.ERROR('ERROR'))