from django.contrib import admin

from luggageshare.apps.luggage.models import Luggage


class LuggageAdmin(admin.ModelAdmin):
    fields = (
        'status', 'user',
        'departure', 'departure_date',
        'arrival', 'arrival_date',
        'size', 'weight', 'weight_mu',
        'price', 'currency', 'note'
    )
    list_display = (
        'status', 'user', 'departure', 'departure_date',
        'arrival', 'arrival_date',
    )

admin.site.register(Luggage, LuggageAdmin)
