# coding: utf-8
from django.conf.urls import url
from django.views.generic import RedirectView

from luggageshare.apps.luggage import views

urlpatterns = [
    url(r'^luggage/(?P<pk>[0-9]+)/update/$', views.LuggageUpdateView.as_view(), name='luggage_update'),
    url(r'^luggage/(?P<pk>[0-9]+)/delete/$', views.LuggageDeleteView.as_view(), name='luggage_delete'),
    url(r'^luggage/(?P<pk>[0-9]+)/$', views.LuggageDetailView.as_view(), name='luggage_detail'),
    url(r'^luggage/create/$', views.LuggageCreateView.as_view(), name='luggage_create'),
    url(r'^luggage/my/$', views.UserLuggageListView.as_view(), name='luggage_user_list'),
    url(r'^luggage/$', views.LuggageListView.as_view(), name='luggage_list'),
    url(r'^$', RedirectView.as_view(url='/luggage/'), name='home'),
]
