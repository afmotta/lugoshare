# coding: utf-8
import datetime
from decimal import Decimal

from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.shortcuts import redirect
from django.views.generic import TemplateView, CreateView, UpdateView, DetailView, ListView
from django.views.generic.edit import FormMixin

from luggageshare.apps.luggage.forms import LuggageForm
from luggageshare.apps.luggage.models import Luggage


class ComingSoon(TemplateView):
    template_name = 'coming_soon.html'


class LuggageCreateView(CreateView):
    model = Luggage
    form_class = LuggageForm
    template_name = 'luggage/luggage_create.html'
    success_url = reverse_lazy('luggage:luggage_list')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(LuggageCreateView, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('luggage:luggage_list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.status = 'published'
        return super(LuggageCreateView, self).form_valid(form)


class LuggageUpdateView(UpdateView):
    form_class = LuggageForm
    template_name = 'luggage/luggage_create.html'
    success_url = reverse_lazy('profile:profile')

    def get_queryset(self):
        return Luggage.objects.filter(user=self.request.user).exclude(status__in=['done', 'removed', 'accepted'])

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(LuggageUpdateView, self).form_valid(form)


class LuggageDetailView(DetailView):
    model = Luggage
    template_name = 'luggage/luggage_detail.html'


class LuggageDeleteView(DetailView):
    template_name = 'luggage/luggage_delete.html'

    def get_queryset(self):
        return Luggage.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        luggage = self.get_object()
        luggage.delete_luggage()
        return redirect('luggage:luggage_list')


class LuggageListView(FormMixin, ListView):
    model = Luggage
    form_class = LuggageForm
    template_name = 'luggage/luggage_list.html'
    paginate_by = 10
    title = 'Luggage list'

    def __init__(self, **kwargs):
        self.filter_dict = None
        self.kg_filter = Q(weight__gte=0, weight_mu__iexact=u'kg')
        self.lbs_filter = Q(weight__gte=0, weight_mu__iexact=u'lbs')
        super(LuggageListView, self).__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.filter_dict = dict()
        self.initial = dict()
        for key in request.GET:
            value = request.GET.get(key, None)
            if value:
                if key in ['departure_date', 'arrival_date']:
                    value = datetime.datetime.strptime(value, '%d/%m/%Y %H:%M')
                    self.filter_dict[key] = value
                elif key in ['weight']:
                    if hasattr(request.user, 'profile'):
                        weight_mu = request.user.profile.weight_mu
                    else:
                        weight_mu = u'kg'
                    if weight_mu == u'kg':
                        converted = round(Decimal(value) * Decimal(0.4535923), 2)
                        self.kg_filter = Q(weight__gte=value, weight_mu__iexact=u'kg')
                        self.lbs_filter = Q(weight__gte=converted, weight_mu__iexact=u'lbs')
                    elif weight_mu == u'lbs':
                        converted = round(Decimal(value) * Decimal(2.204622), 2)
                        self.lbs_filter = Q(weight__gte=value, weight_mu__iexact=u'lbs')
                        self.kg_filter = Q(weight__gte=converted, weight_mu__iexact=u'kg')
                else:
                    self.filter_dict['{}__icontains'.format(key)] = value
                self.initial[key] = value
        return super(LuggageListView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(LuggageListView, self).get_context_data(**kwargs)
        ctx['title'] = self.title
        return ctx

    def get_queryset(self):
        qs = self.model.objects.filter(
            status='published',
            departure_date__gt=datetime.datetime.now(),
            **self.filter_dict
        )
        return qs.filter(self.kg_filter | self.lbs_filter).order_by('-created')


class UserLuggageListView(LuggageListView):
    title = 'My luggage'

    def get_queryset(self):
        return self.model.objects.filter(
            user=self.request.user
        ).exclude(
            status__in=['done', 'removed', 'accepted']
        )
