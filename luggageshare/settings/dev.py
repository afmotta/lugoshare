from .base import *

ADMINS = (
    (os.getenv('DJANGO_ADMIN'), os.getenv('DJANGO_EMAIL')),
)

MANAGERS = ADMINS

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, "static"),
    os.path.join(BASE_DIR, "bower_components"),
)

TEMPLATES[0]['DIRS'].insert(0, os.path.join(PROJECT_DIR, "templates_dev"))

INSTALLED_APPS += ('debug_toolbar',)

# You might want to use sqlite3 for testing in local as it's much faster.
if len(sys.argv) > 1 and 'test' in sys.argv[1]:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': '/tmp/luggageshare_test.db',
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }
