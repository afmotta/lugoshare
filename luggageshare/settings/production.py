from .base import *

ALLOWED_HOSTS = ['*']

ADMINS = (
    (os.getenv('DJANGO_ADMIN'), os.getenv('DJANGO_EMAIL')),
)

MANAGERS = ADMINS

TEMPLATES[0]['DIRS'].insert(0, os.path.join(PROJECT_DIR, "templates_prod"))

INSTALLED_APPS += ('raven.contrib.django.raven_compat',)

RAVEN_CONFIG = {
    'dsn': 'http://7ddd71b07bf0457aa87abee4b1cce5b7:f74aa4dcb56f4955b3a0e139483f724f@sentry.afm.ninja/4',
}
