# coding: utf-8
from decimal import Decimal

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def get_price(context, luggage):
    user = context['user']
    if user.is_authenticated and hasattr(user, 'profile'):
        currency_code = user.profile.currency.code
    else:
        currency_code = 'EUR'
    price = luggage.convert_price(currency_to=currency_code)
    return u'{} {}'.format(price, currency_code)
