# coding: utf-8
from decimal import Decimal

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def get_weight(context, luggage):
    user = context['user']
    if user.is_authenticated and hasattr(user, 'profile'):
        user_mu = user.profile.weight_mu
    else:
        user_mu = u'kg'
    return u'{} {}'.format(luggage.convert_weight(measure_unit=user_mu), user_mu)
